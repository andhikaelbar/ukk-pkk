<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index() {
		$this->load->view('home');
	}

	public function Pengaduan() {
		$this->load->view('pengaduan');	
	}

	public function Tanggapan() {
		$this->load->view('tanggapan');	
	}

	public function Konfirmasi() {
		$this->load->view('konfirmasi');	
	}

	public function Laporan() {
		$this->load->view('laporan');	
	}
}
