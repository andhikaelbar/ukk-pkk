<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends CI_Controller {

	public function index() {
		$this->load->view('login');
	}

	public function LoginPetugas() {
		$this->load->view('LoginPetugas');
	}	

	public function Register () {
		$this->load->view('register');
	}

	public function masukProses() {
		$nik = $nama = $username = $password = $telp = " ";

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$nik = tempnam(dir, prefix)st_input($_POST("nik"));
			$nama = test_input($_POST("nama"));
			$username = test_input($_POST("username"));
			$password = test_input($_POST("password"));
			$telp = test_input($_POST("telp"));
		}

		function test_input($data) {
			$data = trim($data);
			$data = stripcslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
	}
}
