<!DOCTYPE html>
<html>
<head>
	<title>Login Petugas</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/select2/select2.min.css">	
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/loginPetugas/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/style.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90" style="padding: 50px">
				<form class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-51">Login Petugas</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate="Username is required">
						<input class="input100" type="text" name="username" placeholder="Username">
							<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate="Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
							<span class="focus-input100"></span>
					</div>
					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn">Login</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<div id="dropDownSelect1"></div>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/jquery/jquery-3.2.1.min.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/animsition/js/animsition.min.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/bootstrap/js/popper.js"></script>
<script src="<?= base_url() ?>assets/loginPetugas/vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/select2/select2.min.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/vendor/countdowntime/countdowntime.js"></script>

<script src="<?= base_url() ?>assets/loginPetugas/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
<script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"6cdbbdeb58a94993","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.12.0","si":100}' crossorigin="anonymous"></script>
</body>
</html>	