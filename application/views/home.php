<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<div class="container-fluid">
    		<a class="navbar-brand" href="<?= base_url() ?>Dashboard">Pengaduan Masyarakat</a>
    			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      				<span class="navbar-toggler-icon"></span>
    			</button>
    		<div class="collapse navbar-collapse" id="navbarSupportedContent">
      			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        			<li class="nav-item">
          				<a class="nav-link active" aria-current="page" href="<?= base_url() ?>Dashboard/Pengaduan">Pengaduan</a>
        			</li>
        			<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="<?= base_url() ?>Dashboard/Tanggapan">Tanggapan</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="<?= base_url() ?>Dashboard/Konfirmasi">Konfirmasi</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="<?= base_url() ?>Dashboard/Laporan">Laporan</a>
		        	</li>
		      	</ul>
		      		<form class="d-flex">
		        		<a href="<?= base_url() ?>Masuk" class="btn btn-outline-danger" type="submit">Log Out</a>
		      		</form>
		    </div>
		</div>
	</nav>

	<div class="container">
		<h1 style="text-align: center; padding: 20px">Youkoso, Ninggen-domo!</h1>
		<center>You can complain about your problem here, and you can give us suggestion for YangNanya's Village growth!</center>
	</div>
 	
 	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>